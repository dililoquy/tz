Реализовать программу, которая на вход принимает URL
(включая схему, например https://google.com)
на выходе печатает список хэдеров ответа.
Если сервер ответил с отличным от 200 статусом вывести сообщение об ошибке.

пример

./bin/testapp https://google.com

Вывод

```
#Location: http://www.google.com/
#Content-Type: text/html; charset=UTF-8
#Date: Wed, 10 Oct 2018 09:18:29 GMT
#Expires: Fri, 09 Nov 2018 09:18:29 GMT
#Cache-Control: public, max-age=2592000
```

Ключевым является наличие юнит тестов.

Тесты не должны выполнять реальные запросы

Не использовать net/http/httptest.
