package main

import (
	"fmt"
	"net/http"
	"os"
	"strings"
)

type header interface {
	Head(url string) (res *http.Response, err error)
}

func printHelp() {
	fmt.Fprintf(os.Stderr, "Usage:\t %s <url>\n\n", os.Args[0])
}

type tClient struct {
	header
}

type fetchError struct {
	code int
	msg string
}

func NewFetchError(code int, msg string) (err *fetchError) {
	return &fetchError{ code: code, msg: msg }
}

func (e *fetchError) Error() (txt string) {
	return fmt.Sprintf("Fetch error %d: %s\n", e.code, e.msg)
}

func (cli *tClient) fetchHeaders(href string) (headers http.Header, err error) {
	res, err := cli.Head(href)
	if err != nil {
		return
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		err = NewFetchError(res.StatusCode, res.Status)
		return
	}
	headers = res.Header
	return
}

func main() {
	if len(os.Args) != 2 {
		printHelp()
		return
	}
	cli := &tClient{
		&http.Client{},
	}
	headers, err := cli.fetchHeaders(os.Args[1])
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
	for hk, hv := range headers {
		fmt.Printf("#%s: %s\n", hk, strings.Join(hv, ", "))
	}
}
