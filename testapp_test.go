package main

import (
	"testing"
	"net/http"
	"bytes"
	"io"
	"io/ioutil"
)

type mockClient struct{
	*http.Response
}

func (m *mockClient) Head(url string) (res *http.Response, err error) {
	res = m.Response
	return
}

func emptyReadCloser() (rc io.ReadCloser) {
	rc = ioutil.NopCloser(bytes.NewReader(nil))
	return
}

func TestFetchHeaders(t *testing.T) {
	cliSuccess := &tClient{
		&mockClient{
			&http.Response{
				StatusCode: 200,
				Status: "200 OK",
				Header: http.Header{
					"someHeaderKey": []string{ "someHeaderValue" },
				},
				Body: emptyReadCloser(),
			},
		},
	}
	cliFail := &tClient{
		&mockClient{
			&http.Response{
				StatusCode: 404,
				Status: "404 Not Found",
				Body: emptyReadCloser(),
			},
		},
	}
	headers, _ := cliSuccess.fetchHeaders("http://google.com")
	if headers["someHeaderKey"][0] != "someHeaderValue" {
		t.Fatal("failed to fetch headers")
	}
	_, err := cliFail.fetchHeaders("http://google.com");
	if err, ok := err.(*fetchError); !ok || err.code != 404 {
		t.Fatal("failed to report a fetch error")
	}
}
